EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x19_Odd_Even J1
U 1 1 60885331
P 1700 2900
F 0 "J1" H 1750 4017 50  0000 C CNN
F 1 "Conn_02x19_Odd_Even" H 1750 3926 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x19_P2.54mm_Vertical" H 1700 2900 50  0001 C CNN
F 3 "~" H 1700 2900 50  0001 C CNN
	1    1700 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x19_Odd_Even J2
U 1 1 608867A0
P 4700 2900
F 0 "J2" H 4750 4017 50  0000 C CNN
F 1 "Conn_02x19_Odd_Even" H 4750 3926 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x19_P2.54mm_Vertical" H 4700 2900 50  0001 C CNN
F 3 "~" H 4700 2900 50  0001 C CNN
	1    4700 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 2000 1000 2000
Wire Wire Line
	1000 2100 1500 2100
Wire Wire Line
	1500 2200 1000 2200
Wire Wire Line
	1000 2300 1500 2300
Wire Wire Line
	1500 2400 1000 2400
Wire Wire Line
	1000 2500 1500 2500
Wire Wire Line
	1500 2600 1000 2600
Wire Wire Line
	1000 2700 1500 2700
Wire Wire Line
	1500 2800 1000 2800
Wire Wire Line
	1000 2900 1500 2900
Wire Wire Line
	1500 3000 1000 3000
Wire Wire Line
	1000 3100 1500 3100
Wire Wire Line
	1500 3200 1000 3200
Wire Wire Line
	1000 3300 1500 3300
Wire Wire Line
	1500 3400 1000 3400
Wire Wire Line
	1000 3500 1500 3500
Wire Wire Line
	1500 3600 1000 3600
Wire Wire Line
	1000 3700 1500 3700
Wire Wire Line
	1500 3800 1000 3800
Wire Wire Line
	2500 2000 2000 2000
Wire Wire Line
	2000 2100 2500 2100
Wire Wire Line
	2500 2200 2000 2200
Wire Wire Line
	2000 2300 2500 2300
Wire Wire Line
	2500 2400 2000 2400
Wire Wire Line
	2000 2500 2500 2500
Wire Wire Line
	2500 2600 2000 2600
Wire Wire Line
	2000 2700 2500 2700
Wire Wire Line
	2500 2800 2000 2800
Wire Wire Line
	2000 2900 2500 2900
Wire Wire Line
	2500 3000 2000 3000
Wire Wire Line
	2000 3100 2500 3100
Wire Wire Line
	2500 3200 2000 3200
Wire Wire Line
	2000 3300 2500 3300
Wire Wire Line
	2500 3400 2000 3400
Wire Wire Line
	2000 3500 2500 3500
Wire Wire Line
	2500 3600 2000 3600
Wire Wire Line
	2000 3700 2500 3700
Wire Wire Line
	2500 3800 2000 3800
Wire Wire Line
	4500 2000 4000 2000
Wire Wire Line
	4000 2100 4500 2100
Wire Wire Line
	4500 2200 4000 2200
Wire Wire Line
	4000 2300 4500 2300
Wire Wire Line
	4500 2400 4000 2400
Wire Wire Line
	4000 2500 4500 2500
Wire Wire Line
	4500 2600 4000 2600
Wire Wire Line
	4000 2700 4500 2700
Wire Wire Line
	4500 2800 4000 2800
Wire Wire Line
	4000 2900 4500 2900
Wire Wire Line
	4500 3000 4000 3000
Wire Wire Line
	4000 3100 4500 3100
Wire Wire Line
	4500 3200 4000 3200
Wire Wire Line
	4000 3300 4500 3300
Wire Wire Line
	4500 3400 4000 3400
Wire Wire Line
	4000 3500 4500 3500
Wire Wire Line
	4500 3600 4000 3600
Wire Wire Line
	4000 3700 4500 3700
Wire Wire Line
	4500 3800 4000 3800
Wire Wire Line
	5500 2000 5000 2000
Wire Wire Line
	5000 2100 5500 2100
Wire Wire Line
	5500 2200 5000 2200
Wire Wire Line
	5000 2300 5500 2300
Wire Wire Line
	5500 2400 5000 2400
Wire Wire Line
	5000 2500 5500 2500
Wire Wire Line
	5500 2600 5000 2600
Wire Wire Line
	5000 2700 5500 2700
Wire Wire Line
	5500 2800 5000 2800
Wire Wire Line
	5000 2900 5500 2900
Wire Wire Line
	5500 3000 5000 3000
Wire Wire Line
	5000 3100 5500 3100
Wire Wire Line
	5500 3200 5000 3200
Wire Wire Line
	5000 3300 5500 3300
Wire Wire Line
	5500 3400 5000 3400
Wire Wire Line
	5000 3500 5500 3500
Wire Wire Line
	5500 3600 5000 3600
Wire Wire Line
	5000 3700 5500 3700
Wire Wire Line
	5500 3800 5000 3800
Text Label 1100 2000 0    50   ~ 0
PC10
Text Label 1100 2100 0    50   ~ 0
PC12
Text Label 1100 2300 0    50   ~ 0
BOOT0
NoConn ~ 1000 2400
NoConn ~ 1000 2500
NoConn ~ 2500 2400
NoConn ~ 2500 3200
Text Label 1100 2600 0    50   ~ 0
PA13
Text Label 1100 2700 0    50   ~ 0
PA14
Text Label 1100 2800 0    50   ~ 0
PA15
$Comp
L power:GND #PWR?
U 1 1 608CAD14
P 1000 2900
F 0 "#PWR?" H 1000 2650 50  0001 C CNN
F 1 "GND" V 1005 2772 50  0000 R CNN
F 2 "" H 1000 2900 50  0001 C CNN
F 3 "" H 1000 2900 50  0001 C CNN
	1    1000 2900
	0    1    1    0   
$EndComp
Text Label 1100 3000 0    50   ~ 0
PB7
Text Label 1100 3100 0    50   ~ 0
PC13
Text Label 1100 3200 0    50   ~ 0
PC14
Text Label 1100 3300 0    50   ~ 0
PC15
Text Label 1100 3400 0    50   ~ 0
PD0
Text Label 1100 3500 0    50   ~ 0
PD1
$Comp
L power:+BATT #PWR?
U 1 1 608CBFE9
P 1000 3600
F 0 "#PWR?" H 1000 3450 50  0001 C CNN
F 1 "+BATT" V 1015 3727 50  0000 L CNN
F 2 "" H 1000 3600 50  0001 C CNN
F 3 "" H 1000 3600 50  0001 C CNN
	1    1000 3600
	0    -1   -1   0   
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 608D185E
P 1000 2200
F 0 "#PWR?" H 1000 2050 50  0001 C CNN
F 1 "VDD" V 1015 2327 50  0000 L CNN
F 2 "" H 1000 2200 50  0001 C CNN
F 3 "" H 1000 2200 50  0001 C CNN
	1    1000 2200
	0    -1   -1   0   
$EndComp
Text Label 1100 3700 0    50   ~ 0
PC2
Text Label 1100 3800 0    50   ~ 0
PC3
Text Label 2100 3800 0    50   ~ 0
PC0
Text Label 2100 3700 0    50   ~ 0
PC1
Text Label 2100 3600 0    50   ~ 0
PB0
Text Label 2100 3500 0    50   ~ 0
PA4
Text Label 2100 3400 0    50   ~ 0
PA1
Text Label 2100 3300 0    50   ~ 0
PA0
Text Label 2300 3100 0    50   ~ 0
VIN
$Comp
L power:GND #PWR?
U 1 1 608D35DD
P 2500 3000
F 0 "#PWR?" H 2500 2750 50  0001 C CNN
F 1 "GND" V 2505 2872 50  0000 R CNN
F 2 "" H 2500 3000 50  0001 C CNN
F 3 "" H 2500 3000 50  0001 C CNN
	1    2500 3000
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 608D38DC
P 2500 2900
F 0 "#PWR?" H 2500 2650 50  0001 C CNN
F 1 "GND" V 2505 2772 50  0000 R CNN
F 2 "" H 2500 2900 50  0001 C CNN
F 3 "" H 2500 2900 50  0001 C CNN
	1    2500 2900
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 608D3C30
P 2500 2800
F 0 "#PWR?" H 2500 2650 50  0001 C CNN
F 1 "+5V" V 2515 2928 50  0000 L CNN
F 2 "" H 2500 2800 50  0001 C CNN
F 3 "" H 2500 2800 50  0001 C CNN
	1    2500 2800
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 608D3FA6
P 2500 2700
F 0 "#PWR?" H 2500 2550 50  0001 C CNN
F 1 "+3V3" V 2515 2828 50  0000 L CNN
F 2 "" H 2500 2700 50  0001 C CNN
F 3 "" H 2500 2700 50  0001 C CNN
	1    2500 2700
	0    1    1    0   
$EndComp
Text Label 2100 2600 0    50   ~ 0
RESET
Text Label 2100 2500 0    50   ~ 0
IOREF
$Comp
L power:GND #PWR?
U 1 1 608D4ACE
P 2500 2300
F 0 "#PWR?" H 2500 2050 50  0001 C CNN
F 1 "GND" V 2505 2172 50  0000 R CNN
F 2 "" H 2500 2300 50  0001 C CNN
F 3 "" H 2500 2300 50  0001 C CNN
	1    2500 2300
	0    -1   -1   0   
$EndComp
Text Label 2300 2200 0    50   ~ 0
E5V
Text Label 2100 2100 0    50   ~ 0
PD2
Text Label 2100 2000 0    50   ~ 0
PC11
Text Label 4100 2000 0    50   ~ 0
PC9
Text Label 4100 2100 0    50   ~ 0
PB8
Text Label 4100 2200 0    50   ~ 0
PB9
Text Label 4300 2300 0    50   ~ 0
AVdd
$Comp
L power:GND #PWR?
U 1 1 608D6294
P 4000 2400
F 0 "#PWR?" H 4000 2150 50  0001 C CNN
F 1 "GND" V 4005 2272 50  0000 R CNN
F 2 "" H 4000 2400 50  0001 C CNN
F 3 "" H 4000 2400 50  0001 C CNN
	1    4000 2400
	0    1    1    0   
$EndComp
Text Label 4100 2500 0    50   ~ 0
PA5
Text Label 4100 2600 0    50   ~ 0
PA6
Text Label 4100 2700 0    50   ~ 0
PA7
Text Label 4100 2800 0    50   ~ 0
PB6
Text Label 4100 2900 0    50   ~ 0
PC7
Text Label 4100 3000 0    50   ~ 0
PA9
Text Label 4100 3100 0    50   ~ 0
PA8
Text Label 4100 3200 0    50   ~ 0
PB10
Text Label 4100 3300 0    50   ~ 0
PB4
Text Label 4100 3400 0    50   ~ 0
PB5
Text Label 4100 3500 0    50   ~ 0
PB3
Text Label 4100 3600 0    50   ~ 0
PA10
Text Label 4100 3700 0    50   ~ 0
PA2
Text Label 4100 3800 0    50   ~ 0
PA3
Text Label 5100 2000 0    50   ~ 0
PC8
Text Label 5100 2100 0    50   ~ 0
PC6
Text Label 5100 2200 0    50   ~ 0
PC5
Text Label 5300 2300 0    50   ~ 0
U5V
NoConn ~ 5500 2400
NoConn ~ 5500 3800
NoConn ~ 5500 3700
Text Label 5100 2500 0    50   ~ 0
PA12
Text Label 5100 2600 0    50   ~ 0
PA11
Text Label 5100 2700 0    50   ~ 0
PB12
Text Label 5100 2800 0    50   ~ 0
PB11
Text Label 5100 3000 0    50   ~ 0
PB2
Text Label 5100 3100 0    50   ~ 0
PB1
Text Label 5100 3200 0    50   ~ 0
PB15
Text Label 5100 3300 0    50   ~ 0
PB14
Text Label 5100 3400 0    50   ~ 0
PB13
Text Label 5100 3600 0    50   ~ 0
PC4
Text Label 5300 3500 0    50   ~ 0
AGND
Text Notes 1350 2250 0    25   ~ 0
(1)
Text Notes 1300 2550 0    25   ~ 0
(3)
Text Notes 1300 2650 0    25   ~ 0
(3)
Text Notes 2250 3650 0    25   ~ 0
(4)
Text Notes 2250 3750 0    25   ~ 0
(4)
Text Notes 5450 2250 0    25   ~ 0
(2)
Text Notes 1000 4500 0    50   ~ 0
1: Default state of BOOT0 is LOW\n2: U5V is 5V power from ST-Link USB connector\n3: Pins shared with the SWD signals from ST-Link\n4: ADC pins via solder bridge
$EndSCHEMATC
